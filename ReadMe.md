## **Face Detection CLI Application using facenet-pytorch**

- To use CUDA accelration with pytorch, uncomment lines 12 and 13 and comment line 15

- Works by default to process images

<br>

### **CLI Arguments:**
<pre>
1. --video, -v     : Flag that controls entry to perform detection on a video file
2. --realtime, -rt : Flag that controls entry to perform realtime detection
3. --all, -a       : Flag that controls whether to detect all faces in the image/video
4. --file, -f      : Name of the file (Used for images and videos)
5. --size, -s      : Image size to be used by the model (Default: 160)
6. --downscale, -d : Used to downscale the video file (Useful for display purposes)
</pre>
