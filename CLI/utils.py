import os
import cv2
import platform
import numpy as np
import matplotlib.pyplot as plt

from PIL import Image
from facenet_pytorch import MTCNN

READ_PATH = "Files"

# import torch
# DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

DEVICE = "cpu"


if not os.path.exists(READ_PATH):
    os.makedirs(READ_PATH)


def init_webcam():
    if platform.system() != "Windows":
        cap = cv2.VideoCapture(0)
    else:            
        cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 360)
    cap.set(cv2.CAP_PROP_FPS, 30)

    return cap

#####################################################################################################

class ImageHandler(object):
    def __init__(self):
        pass

    def read_image(self, path: str) -> np.ndarray:
        return cv2.imread(path, cv2.IMREAD_COLOR)


    def preprocess(self, image: np.ndarray) -> np.ndarray:
        return cv2.cvtColor(src=image, code=cv2.COLOR_BGR2RGB)


    def downscale(self, image: np.ndarray, factor: float) -> np.ndarray:
        h, w, _ = image.shape
        return cv2.resize(src=image, dsize=(int(w/factor), int(h/factor)), interpolation=cv2.INTER_AREA)


    def show(self, image: np.ndarray, title=None) -> None:
        plt.figure(figsize=(8, 6))
        plt.imshow(image)
        plt.axis("off")
        if title:
            plt.title(title)
        figmanager = plt.get_current_fig_manager()
        figmanager.window.state("zoomed")
        plt.show()

image_handler = ImageHandler()

#####################################################################################################

class DrawBoxes(object):
    def __init__(self):
        pass

    def draw_single_box(self, image: np.ndarray, x1: int, y1: int, x2: int, y2: int, thickness: int) -> None:
        cv2.rectangle(img=image, pt1=(x1, y1), pt2=(x2, y2), color=(0, 255, 0), thickness=thickness)


    def draw_all_boxes(self, image: np.ndarray, x1: list, y1: list, x2: list, y2: list, thickness: int) -> np.ndarray:
        for i in range(len(x1)):
            cv2.rectangle(img=image, pt1=(x1[i], y1[i]), pt2=(x2[i], y2[i]), color=(0, 255, 0), thickness=thickness)

draw_boxes = DrawBoxes()

#####################################################################################################

class FaceDetection(object):
    def __init__(self, size: int):
        self.model = MTCNN(image_size=size, keep_all=True, device=DEVICE)

    def downsample(self, image: np.ndarray, size: int) -> np.ndarray:
        return cv2.resize(src=image, dsize=(size, size), interpolation=cv2.INTER_AREA)

    def single_face_detect(self, image: np.ndarray, size: int):
        x1, y1, x2, y2 = None, None, None, None
        h, w, _ = image.shape
        pil_image = Image.fromarray(self.downsample(image, size))
        boxes, _ = self.model.detect(pil_image)

        if boxes is not None:
            index = 0 
            x1, y1, x2, y2 = int(boxes[index][0] * (w/size)), \
                            int(boxes[index][1] * (h/size)), \
                            int(boxes[index][2] * (w/size)), \
                            int(boxes[index][3] * (h/size))
        return x1, y1, x2, y2

    def face_detect(self, image: np.ndarray, size: int):
        x1, y1, x2, y2 = [], [], [], []
        h, w, _ = image.shape
        pil_image = Image.fromarray(self.downsample(image, size))
        boxes, _ = self.model.detect(pil_image)

        if boxes is not None:
            for box in boxes:
                x1.append(int(box[0] * (w/size))), 
                y1.append(int(box[1] * (h/size))), 
                x2.append(int(box[2] * (w/size))), 
                y2.append(int(box[3] * (h/size)))
        return x1, y1, x2, y2


def get_model(size: int):
    model = FaceDetection(size)
    return model

#####################################################################################################

