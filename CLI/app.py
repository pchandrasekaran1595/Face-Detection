import sys
from os import listdir
from cv2 import VideoCapture, CAP_PROP_POS_FRAMES, imshow, waitKey, destroyAllWindows

from .utils import READ_PATH, image_handler, draw_boxes, get_model, init_webcam


def breaker(num=50, char="*"):
    print("\n" + num*char + "\n")


def run():
    args_1 = ["--video", "-v"]
    args_2 = ["--realtime", "-rt"]
    args_3 = ["--all", "-a"]
    args_4 = ["--file", "-f"]
    args_5 = ["--size", "-s"]
    args_6 = ["--downscale", "-d"]

    do_image = True
    do_video = False
    do_realtime = False
    detect_all = False
    filename = None
    size = 160
    downscale_factor = None

    if args_1[0] in sys.argv or args_1[1] in sys.argv: do_image, do_video, do_realtime = False, True, False
    if args_2[0] in sys.argv or args_2[1] in sys.argv: do_image, do_video, do_realtime = False, False, True
    if args_3[0] in sys.argv or args_3[1] in sys.argv: detect_all = True

    if args_4[0] in sys.argv: filename = sys.argv[sys.argv.index(args_4[0]) + 1]
    if args_4[1] in sys.argv: filename = sys.argv[sys.argv.index(args_4[1]) + 1]

    if args_5[0] in sys.argv: size = sys.argv[sys.argv.index(args_5[0]) + 1]
    if args_5[1] in sys.argv: size = sys.argv[sys.argv.index(args_5[1]) + 1]

    if args_6[0] in sys.argv: downscale_factor = float(sys.argv[sys.argv.index(args_6[0]) + 1])
    if args_6[1] in sys.argv: downscale_factor = float(sys.argv[sys.argv.index(args_6[1]) + 1])

    model = get_model(size)

    if do_image:
        assert filename is not None, "Enter argument for --file | -f"
        assert filename in listdir(READ_PATH), "File Not Found"

        image = image_handler.preprocess(image_handler.read_image(READ_PATH + "/" + filename))
        disp_image = image.copy()

        if detect_all:
            x1, y1, x2, y2 = model.face_detect(image, size)
            if len(x1) == 0:
                breaker()
                print("No Faces Detected")
                breaker()
            else:
                draw_boxes.draw_all_boxes(disp_image, x1, y1, x2, y2, 3)
                image_handler.show(disp_image, "Detections")
        else:
            x1, y1, x2, y2 = model.single_face_detect(image, size)
            if x1 is None:
                breaker()
                print("No Faces Detected")
                breaker()
            else:
                draw_boxes.draw_single_box(disp_image, x1, y1, x2, y2, 3)
                image_handler.show(disp_image, "Detections")
    
    if do_video:
        assert filename is not None, "Enter argument for --file | -f"
        assert filename in listdir(READ_PATH), "File Not Found"

        cap = VideoCapture(READ_PATH + "/" + filename)
        while cap.isOpened():
            ret, frame = cap.read()

            if ret:
                if downscale_factor is not None:
                    frame = image_handler.downscale(frame, downscale_factor)
                
                disp_frame = frame.copy()
                frame = image_handler.preprocess(frame)

                if detect_all:
                    x1, y1, x2, y2 = model.face_detect(frame, size)
                    if len(x1) == 0:
                        pass
                    else:
                        draw_boxes.draw_all_boxes(disp_frame, x1, y1, x2, y2, 2)
                else:
                    x1, y1, x2, y2 = model.single_face_detect(frame, size)
                    if x1 is None:
                        pass
                    else:
                        draw_boxes.draw_single_box(disp_frame, x1, y1, x2, y2, 2)

                imshow("Face Detections", disp_frame)
                if waitKey(1) == ord("q"):
                    break
    
            else:
                cap.set(CAP_PROP_POS_FRAMES, 0)
        
        cap.release()
        destroyAllWindows()
    
    if do_realtime:
        cap = init_webcam()

        while cap.isOpened():
            _, frame = cap.read()
            disp_frame = frame.copy()

            frame = image_handler.preprocess(frame)

            if detect_all:
                x1, y1, x2, y2 = model.face_detect(frame, size)
                if len(x1) == 0:
                    pass
                else:
                    draw_boxes.draw_all_boxes(disp_frame, x1, y1, x2, y2, 2)
            else:
                x1, y1, x2, y2 = model.single_face_detect(frame, size)
                if x1 is None:
                    pass
                else:
                    draw_boxes.draw_single_box(disp_frame, x1, y1, x2, y2, 2)

            imshow("Face Detections", disp_frame)
            if waitKey(1) == ord("q"):
                break
        
        cap.release()
        destroyAllWindows()
